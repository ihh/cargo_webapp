export class User {
    id: number;
    code: string = '';
    costAccount: string = '';
    email: string = '';
    enabled: boolean = false;
    faxNumber: string = '';
    financeAccount: string = '';
    firstName: string = '';
    lastName: string = '';
    phoneNumber: string = '';
    registrationNumber: string = '';
    username: string = '';
}