import { CargoWebappPage } from './app.po';

describe('cargo-webapp App', function() {
  let page: CargoWebappPage;

  beforeEach(() => {
    page = new CargoWebappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
